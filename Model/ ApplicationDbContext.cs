using Microsoft.EntityFrameworkCore;

public class  ApplicationDbContext : DbContext
{
    public DbSet<User> Users { get; set; }
    public DbSet<Location> Locations { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseNpgsql("Host=localhost;Database=postgresGoogleMaps;Username=postgres;Password=postgres");
    }
}